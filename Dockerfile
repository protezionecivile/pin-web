FROM node:lts-slim as node-stage

RUN apt-get update -q && \
    apt-get install -q -y git openssh-client

# add credentials to access private repos

ARG SSH_PRIVATE_KEY
ARG SSH_PUBLIC_KEY
RUN mkdir ~/.ssh/
RUN echo "${SSH_PRIVATE_KEY}" > ~/.ssh/id_rsa
RUN echo "${SSH_PUBLIC_KEY}" > ~/.ssh/id_rsa.pub
RUN chmod 400 ~/.ssh/id_rsa
# make sure your domain is accepted
RUN ssh-keyscan gitlab.com >> ~/.ssh/known_hosts

RUN cat ~/.ssh/id_rsa
RUN cat ~/.ssh/id_rsa.pub

#RUN ssh -T git@gitlab.com
#RUN ls -la ~/.ssh

# create app directory
WORKDIR /opt/pin-web_build

# Bundle app source
COPY . .

RUN npm i
RUN npm run build:prod-norefresh

###########
## stage-2
############
FROM nginx:latest

RUN rm -rf /usr/share/nginx/html/*

WORKDIR /usr/share/nginx/html

COPY conf/pinweb-nginx.conf /etc/nginx/conf.d/default.conf
#COPY conf/ngix.conf /etc/nginx/nginx.conf

COPY --from=node-stage /opt/pin-web_build/dist/pin-web .

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]

