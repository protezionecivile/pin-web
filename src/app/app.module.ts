/**
 * pin-web
 *
 * Amaca CivicEvidence
 * (c) 2019 by Sciamlab s.r.l.
 *
 * The PIN application is part of the Amaca CivicEvidence Platform
 *
 */
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, LOCALE_ID, APP_INITIALIZER } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CommonModule, Location } from '@angular/common';


import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { StaticModule } from './modules/static/static.module';
import { NgMapModule } from '@sciamlab/ng-map';

import { SnackbarService, NgCommonModule } from '@sciamlab/ng-common';
import { CONFIG } from 'src/config';
import { ModuleWithProviders } from '@angular/core';
import { NavigatorComponent } from './components/navigator/navigator.component';
import { HomeComponent } from './components/home/home.component';
import { PINControlSearchComponent } from './components/control-search/control-search.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { LoaderComponent } from './components/loader/loader.component';
import { LoaderService } from './components/loader/loader.service';
import { LoaderInterceptor } from './components/loader/loader.interceptor';
import { InfoboxComponent } from './components/home/infobox/infobox.component';
import { FunnelComponent } from './components/home/infobox/funnel/funnel.component';

import { registerLocaleData } from '@angular/common';
import localeIt from '@angular/common/locales/it';
import { RadialGraphComponent } from './components/home/infobox/radial-graph/radial-graph.component';
import { NgScrollbarModule } from 'ngx-scrollbar';
import { SmoothScrollModule } from 'ngx-scrollbar/smooth-scroll';
import { ReplacePipe } from './directive/replace.pipe';
import { DeviceDetectorModule } from 'ngx-device-detector';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { ControlLayerfilterComponent } from './components/control-layerfilter/control-layerfilter.component';
import { CookieService } from 'ngx-cookie-service';
import { PINHelpModalComponent } from './components/help-modal/help-modal.component';
import { ControlShareComponent } from './components/control-share/control-share.component';
import { CookieBarComponent } from './components/cookie-bar/cookie-bar.component';

// the second parameter 'fr' is optional
registerLocaleData(localeIt, 'it-IT');


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavigatorComponent,
    PINControlSearchComponent,
    LoaderComponent,
    InfoboxComponent,
    FunnelComponent,
    RadialGraphComponent,
    ReplacePipe,
    ControlLayerfilterComponent,
    PINHelpModalComponent,
    ControlShareComponent,
    CookieBarComponent,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    NgbModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FontAwesomeModule,
    NgMapModule.forRoot({
      wfs: CONFIG.geoserver.wfs
    }),
    DragDropModule,
    NgCommonModule.forRoot({
      snackbar: CONFIG.snackbar
    }),
    StaticModule,
    AppRoutingModule,
    NgScrollbarModule,
    SmoothScrollModule,
    DeviceDetectorModule.forRoot(),
    ServiceWorkerModule.register('./ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [
    LoaderService,
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true },
    CookieService
  ],
  entryComponents: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
