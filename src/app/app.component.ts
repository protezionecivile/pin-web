/**
 * pin-web
 *
 * Amaca CivicEvidence
 * (c) 2019 by Sciamlab s.r.l.
 *
 * The PIN application is part of the Amaca CivicEvidence Platform
 *
 */
import {Component, OnDestroy, OnInit, Renderer2, HostBinding, AfterViewChecked, AfterViewInit} from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router , ActivationStart} from '@angular/router';
import { AbstractComponent, SnackbarService } from '@sciamlab/ng-common';
import { Location } from '@angular/common';
import { CookieService } from 'ngx-cookie-service';

declare var $: any;
declare let gtag: Function;

@Component({
  selector: 'body',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent extends AbstractComponent implements OnInit, OnDestroy {
  @HostBinding('class') public cssClass = '';

  navigationSubscription;

  constructor(
    route: ActivatedRoute,
    router: Router,
    snackbarService: SnackbarService,
    private renderer: Renderer2,
    private cookieService: CookieService
 /*   private location: Location,*/
  ) {
    super(route, router, snackbarService, null);

//     this.router.events
//       .subscribe((event) => {
//         if (event instanceof ActivationStart) {
//           console.log(event);
// //          if (event.snapshot. !== '/') {
// //            this.renderer.addClass(document.body, 'static-page');
// //          }
//         }
//       });
  }

  async ngOnInit() {
    try {

      this.navigationSubscription = this.router.events.subscribe((e: any) => {
        // If it is a NavigationEnd event re-initalise the component
        if (e instanceof NavigationEnd) {
          gtag('config', 'UA-38033959-10', {page_path: e.urlAfterRedirects});
        }
      });

    } catch (error) {
      console.error(error);
      this.snackbarService.error(error.message || error);
    }
  }
  ngOnDestroy() {
    // avoid memory leaks here by cleaning up after ourselves
    // If we don't then we will continue to run our initialiseInvites() method on every navigationEnd event.
    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
    }
  }
}
