import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import * as fa from '@fortawesome/free-solid-svg-icons';
import { NgMapService } from '@sciamlab/ng-map';
import { Map } from 'ol/Map';
import { BaseLayer } from 'ol/layer/Base';
import TileLayer from 'ol/layer/Tile';
import TileWMS from 'ol/source/TileWMS';

import VectorTileLayer from 'ol/layer/VectorTile';
import VectorTileSource from 'ol/source/VectorTile';

@Component({
  selector: 'pin-map-control-layerfilter',
  templateUrl: './control-layerfilter.component.html',
  styleUrls: ['./control-layerfilter.component.scss']
})
export class ControlLayerfilterComponent implements OnInit {

  fa = fa;
  public filterButtons = [
    { displayLabel: 'Comma 1028', label: 'LN145', cssState: 'selected' },
    { displayLabel: 'Art. 24-quater', label: 'DL119', cssState: 'selected' }
  ];

  public activeFinancingSource: Array<string> = ['LN145', 'DL119'];

  public map: Map;

  interventiTileLayer: TileLayer;
  interventiMVTLayerLN145: VectorTileLayer;
  interventiMVTLayerDL119: VectorTileLayer;

  @Output() filterEvent = new EventEmitter<any>();

  constructor(private mapService: NgMapService) { }

  ngOnInit() {
    this.map = this.mapService.getMap('map');
    this.map.getLayers().forEach((lyr: BaseLayer) => {
      if (lyr.get('id') === 'interventi_wms') {
        this.interventiTileLayer = lyr;
      } else if (lyr.get('id') === 'interventi_mvtLN145') {
        this.interventiMVTLayerLN145 = lyr;
      } else if (lyr.get('id') === 'interventi_mvtDL119') {
        this.interventiMVTLayerDL119 = lyr;
      }
    });
  }


  onClick(thisbutton) {
    // non deve essere possibile disattivare tutti i pulsanti
    // nel caso il corrente è l'ultimo attivo non sarà possibile disattivarlo

    let isLastActive: boolean = true;

    for (let b of this.filterButtons) {
      if (b.label !== thisbutton.label && b.cssState === 'selected') {
        isLastActive = false;
        break;
      }
    }

    //(thisbutton.cssState === 'selected' && !isLastActive) ? thisbutton.cssState = '' : thisbutton.cssState = 'selected';
    let cql_params = { CQL_FILTER: ''};

    if (thisbutton.cssState === 'selected' && !isLastActive) {
      // il pulsante cliccato è attivo e non è l'ultimo attivo
      // quindi posso disattivarlo
      thisbutton.cssState = '';
      if (thisbutton.label === 'LN145') {
        this.interventiMVTLayerLN145.setVisible(false);
        cql_params.CQL_FILTER = 'FONTE_FINANZIAMENTO=\'DL119\'';
      } else if (thisbutton.label === 'DL119') {
        this.interventiMVTLayerDL119.setVisible(false);
        cql_params.CQL_FILTER = 'FONTE_FINANZIAMENTO=\'LN145\'';
      }
      this.interventiTileLayer.getSource().updateParams(cql_params);
    } else {
      // il pulsante selezionato o era attivo ma anche l'ultimo attivo in tal caso non devo fare nulla
      // oppure era disattivato e quindi adesso devo attivarlo
      if (thisbutton.cssState === '') {
        thisbutton.cssState = 'selected';
        if (thisbutton.label === 'LN145') {
          this.interventiMVTLayerLN145.setVisible(true);
        } else if (thisbutton.label === 'DL119') {
          this.interventiMVTLayerDL119.setVisible(true);
        }

        // in base ai layer attivi ricompongo la query CQL
        cql_params.CQL_FILTER = '';
        for (let b of this.filterButtons) {
          if (b.cssState === 'selected') {
            cql_params.CQL_FILTER += 'OR FONTE_FINANZIAMENTO=\'' + b.label + '\'';
          }
        }
        cql_params.CQL_FILTER = cql_params.CQL_FILTER.slice(3);
        this.interventiTileLayer.getSource().updateParams(cql_params);
      }
    }

    this.activeFinancingSource = new Array();
    for (let b of this.filterButtons) {
      if (b.cssState === 'selected') {
        this.activeFinancingSource.push(b.label);
      }
    }

    // emit the active financing sources
    this.filterEvent.emit(this.activeFinancingSource);
  }

}
