import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AbstractComponent, SnackbarService} from '@sciamlab/ng-common';
import {TranslateService} from '@ngx-translate/core';
import {CookieService} from 'ngx-cookie-service';

declare var $: any;

@Component({
  selector: 'app-cookie-bar',
  templateUrl: './cookie-bar.component.html',
  styleUrls: ['./cookie-bar.component.scss']
})
export class CookieBarComponent extends AbstractComponent implements OnInit {

  constructor(
    route: ActivatedRoute,
    router: Router,
    snackbarService: SnackbarService,
    translate: TranslateService,
    private cookieService: CookieService
  ) {
    super(route, router, snackbarService, translate);
  }

  ngOnInit() {
    try {
      if (this.cookieService.get('cookies_consent') === '') {
        $('.cookiebar').addClass('show');
      }
    } catch (error) {
      console.error(error);
      this.snackbarService.error(error.message || error);
    }
  }


}
