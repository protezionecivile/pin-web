import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import * as fa from '@fortawesome/free-solid-svg-icons';
import { AngularWaitBarrier } from 'blocking-proxy/built/lib/angular_wait_barrier';

@Component({
  selector: 'pin-app-control-share',
  templateUrl: './control-share.component.html',
  styleUrls: ['./control-share.component.scss']
})
export class ControlShareComponent implements OnInit {

  fa = fa;
  currentPath = '';
  webNavigator: any = null;

  constructor( private location: Location) {
    this.webNavigator = window.navigator;
  }

  ngOnInit() {
    this.location.onUrlChange( (url) => {
      this.currentPath = url;
    });

  }

  onShare() {
    console.log(this.currentPath)
    if (this.webNavigator !== null && this.webNavigator.share !== undefined ) {
       this.share({title: '', text: 'Piano Interventi Nazionale - Protezione Civile', url: this.currentPath}).then( (data:any) => {
        if (data.error) {
          console.log('errore nello share: ', data.error);
        } else {
          console.log('share ok: ', data.shared);
        }
       });
    } else {
      console.log('Share non possibile');
    }
    // if (this.nav) {
    //   this.nav.share({
    //     title: 'PIN Condividi',
    //     text: '',
    //     url: this.currentPath
    //   })
    //   .then(() => { console.log('Thanks for sharing!'); })
    //   .catch(console.error);
    // } else {
    //   alert('share not supported');
    // }

  }

  share({title, text, url }: { title: string, text?: string, url?: string }) {
    return new Promise(async (resolve, reject) => {
      if (this.webNavigator !== null && this.webNavigator.share !== undefined) {
        if (
          (text === undefined || text === null) &&
          (url === undefined || url === null)
        ) {
          console.warn(`text and url both can't be empty, at least provide either text or url`);
        } else {
          try {
            const isShared = await this.webNavigator.share({
              title: title,
              text: text,
              url: url,
            });
            resolve({
              shared: true
            });
          } catch (err) {
            reject({
              shared: false,
              error: err
            });
          }
        }
      } else {
        reject({
          shared: false,
          error: `This service/api is not supported in your Browser`
        });
      }
    });
  }
}
