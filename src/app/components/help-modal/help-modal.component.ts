import {AfterViewInit, Component, OnInit} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AbstractComponent, SnackbarService } from '@sciamlab/ng-common';
import {CookieService} from 'ngx-cookie-service';

declare var $: any;

@Component({
  selector: 'pin-help-modal',
  templateUrl: './help-modal.component.html',
  styleUrls: ['./help-modal.component.scss'],
})
export class PINHelpModalComponent extends AbstractComponent implements OnInit, AfterViewInit{

  cookieValue: string ;

  constructor(
    route: ActivatedRoute,
    router: Router,
    snackbarService: SnackbarService,
    private cookieService: CookieService
  ) {
    super(route, router, snackbarService, null);
  }

  ngOnInit() {
    try {
    } catch (error) {
      console.error(error);
      this.snackbarService.error(error.message || error);
    }
  }
  ngAfterViewInit(): void {
    this.cookieValue = this.cookieService.get('returned');
    //console.log(this.cookieValue);
    if (this.cookieValue === '') {
      $('#help-modal').modal();
      //this.cookieService.set(<cookie name>,<value>,<expiry num = num of dayes || date = till specific date> )
      this.cookieService.set('returned', 'true', 1);
    }
  }
}
