import { Component, ElementRef, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AbstractComponent, SnackbarService } from '@sciamlab/ng-common';

@Component({
  selector: 'app-navigator',
  templateUrl: './navigator.component.html',
  styleUrls: ['./navigator.component.scss'],
  host: {
    '(document:click)': 'onClick($event)',
  },
})

export class NavigatorComponent extends AbstractComponent implements OnInit {

  constructor(
    route: ActivatedRoute,
    router: Router,
    snackbarService: SnackbarService,
    private el: ElementRef
  ) {
    super(route, router, snackbarService, null);
  }

  async ngOnInit() {
    try {

    } catch (error) {
      console.error(error);
      this.snackbarService.error(error.message || error);
    }
  }

  onClick(event) {
    if (['HTMLSpanElement', 'HTMLAnchorElement'].includes(event.target.constructor.name)) {
      ((this.el.nativeElement as HTMLElement).querySelectorAll('#primaryNav .overlay')[0] as HTMLElement).click();
    }
  }
}
