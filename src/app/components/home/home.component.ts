import { Component, OnInit, OnDestroy } from '@angular/core';
import { AbstractComponent, SnackbarService } from '@sciamlab/ng-common';
import { ActivatedRoute, Router, Event, NavigationEnd } from '@angular/router';
import { CONFIG, MACROREGIONI, PROVINCE, REGIONI, COMUNI, INTERVENTI, NS, ITALIA, SHOW_ATTRIBUTION } from 'src/config';
import { NgMapService, WFSService } from '@sciamlab/ng-map';
import { LoaderService } from '../loader/loader.service';
import { PathLocationStrategy, LocationStrategy, Location } from '@angular/common';
import { CdkDragEnd, CdkDragStart, CdkDragMove } from '@angular/cdk/drag-drop';
import { DeviceDetectorService } from 'ngx-device-detector';
import { AppComponent } from 'src/app/app.component';

declare let gtag: Function;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [Location, { provide: LocationStrategy, useClass: PathLocationStrategy }],
  host: {
    '(window:resize)': 'onResize($event)'
  }
})

export class HomeComponent extends AbstractComponent implements OnInit, OnDestroy {

  isOpen = false;
  isAnimated = false;
  layers: { type: string, options: any }[] = [];
  view_options;
  extent: number[][];
  selected: any;
  intervento: any;
  dragged: any;
  error_code: number;
  INTERVENTI = INTERVENTI;
  PROVINCE = PROVINCE;
  MACROREGIONI = MACROREGIONI;
  REGIONI = REGIONI;
  ITALIA = ITALIA;
  dragPosition = { x: 0, y: 0 };
  isMobile = false;
  deviceInfo;
  simog = CONFIG.simog_url;
  enableDrag = true;
  active_financig_source = ["LN145", "DL119"];
  importo_interventi: number = 0.0;
  nr_interventi: number = 0;
  totale_assegnato: number = 0.0;
  sal: number = 0.0;
  pagamenti: number = 0.0;
  showAttribution: boolean = SHOW_ATTRIBUTION;

  url_params = {
    m: MACROREGIONI,
    r: REGIONI,
    p: PROVINCE,
    c: COMUNI,
    i: INTERVENTI
  };

  url_params_reverse = {};

  navigationSubscription;
  mouseClickSubscription;

  italia;

  constructor(
    route: ActivatedRoute,
    router: Router,
    snackbarService: SnackbarService,
    private mapService: NgMapService,
    private wfsService: WFSService,
    private loader: LoaderService,
    private location: Location,
    private deviceService: DeviceDetectorService,
    private rootApp: AppComponent
  ) {
    super(route, router, snackbarService, null);
    rootApp.cssClass = '';
    this.url_params_reverse[MACROREGIONI] = 'm';
    this.url_params_reverse[REGIONI] = 'r';
    this.url_params_reverse[PROVINCE] = 'p';
    this.url_params_reverse[COMUNI] = 'c';
    this.url_params_reverse["int_newLN145"] = 'i';
    this.url_params_reverse["int_newDL119"] = 'i';
    this.deviceInfo = this.deviceService.getDeviceInfo();
    this.isMobile = !this.deviceService.isDesktop();
  }

  async ngOnInit() {
    try {

      this.view_options = CONFIG.view;
      // base map
      // this.layers.push({ type: CONFIG.default.basemap, source: CONFIG.base_layers[CONFIG.default.basemap] });
      this.layers.push({ type: 'xyz', options: CONFIG.geoserver.layers.skobbler });
      this.layers.push({ type: 'xyz', options: CONFIG.geoserver.layers.gmaps });
      //this.layers.push({ type: 'bing', options: CONFIG.geoserver.layers.bing });
      //this.layers.push({ type: 'wms', options: CONFIG.geoserver.layers.realvista });
      // other layers
      this.layers.push({ type: 'wms', options: CONFIG.geoserver.layers.interventi_wms });
      // this.layers.push({ type: 'xyz', options: CONFIG.geoserver.layers.esri_grey_labels });
      this.layers.push({ type: 'mvt', options: CONFIG.geoserver.layers.macroregioni });
      this.layers.push({ type: 'mvt', options: CONFIG.geoserver.layers.regioni });
      this.layers.push({ type: 'mvt', options: CONFIG.geoserver.layers.province });
      this.layers.push({ type: 'mvt', options: CONFIG.geoserver.layers.comuni });
      this.layers.push({ type: 'mvt', options: CONFIG.geoserver.layers.interventi_mvt_LN145 });
      this.layers.push({ type: 'mvt', options: CONFIG.geoserver.layers.interventi_mvt_DL119 });

      this.mouseClickSubscription = this.mapService.mouseClick$.subscribe(features => {
        if (features) {
          if (features.length > 0 && features[0].properties_.layer !== ITALIA) {
            // se ci sono features selezionate che non sono l'italia
            const properties = Object.assign({}, features[0].properties_);

            let id = features[0].id_;
            if (properties.layer.indexOf(INTERVENTI) >= 0) {
              properties.layer = INTERVENTI + properties.FONTE_FINANZIAMENTO;
              id = properties.id_int;
              properties.nome = 'Intervento';

            }
            if (!this.url_params_reverse[properties.layer]) {
              console.log(properties.layer);
              this.snackbarService.error(`Layer type not recognized: ${properties.layer}`);

            } else {
              this.selectFeature(properties);
              gtag('config', 'UA-38033959-10',{'page_path': `/${this.url_params_reverse[properties.layer]}/${id}`});
              this.location.go(`/${this.url_params_reverse[properties.layer]}/${id}`);
              
            }
          } else {
            // altrimenti è l'italia quindi la seleziono
            this.selectFeature(Object.assign({}, this.italia.properties_));
            this.location.go('');
        }
        } else {
          // altrimenti è l'italia quindi la seleziono
          this.location.go('');
          this.selectFeature(Object.assign({}, this.italia.properties_));
        }
      });


      this.navigationSubscription = this.router.events.subscribe((e: any) => {
        // If it is a NavigationEnd event re-initalise the component
        if (e instanceof NavigationEnd) {
          console.log('reload component');
          this.loadSelectedFromURL();
        }
      });

      this.italia = await this.wfsService.getFeatureById(NS, ITALIA, '1', ['breadcrumb',
        'extent',
        'nome',
        'osmid',
        'uid',
        'totale_assegnato',
        'totale_interventi_pubblici',
        'nr_interventi_pubblici',
        'sal',
        'pagamenti',
        'funnel_data']).toPromise();
      this.updateDisplay();
      this.loadSelectedFromURL();

    } catch (error) {
      console.error(error);
      this.snackbarService.error(error.message || error);
    }
  }

  loadSelectedFromURL() {
    console.log("load from url");    
    this.loader.show();
    if (this.route.snapshot.params.layer && this.route.snapshot.params.id) {
      const layer_type = this.url_params[this.route.snapshot.params.layer];
      if (!layer_type) {
        this.error_code = 404;
        this.loader.hide();

      } else {
        this.wfsService.getFeatureById(NS, layer_type, this.route.snapshot.params.id).toPromise()
          .then((feature: any) => {
            if (feature) {
              if (feature.properties_ && feature.properties_.layer.indexOf(INTERVENTI) >= 0) {
                feature.properties_.extent = JSON.stringify([
                  [feature.properties_.LON_4326, feature.properties_.LAT_4326],
                  [feature.properties_.LON_4326, feature.properties_.LAT_4326]
                ]);
              }
              this.mapService.emitFeatureSelect([feature]);
              this.loader.hide();

            } else {
              this.error_code = 404;
              this.loader.hide();
            }
          })
          .catch(err => { throw err; });
      }

    } else {
      this.mapService.emitFeatureSelect([this.italia]);
      this.loader.hide();
    }
  }

  selectFeature(properties) {
    console.log('SELECTED-FEATURE: ', properties);
    properties.breadcrumb = properties.breadcrumb ? JSON.parse(properties.breadcrumb) : [];
    properties.CIG_JSON = JSON.parse(properties.CIG_JSON || '{}');
    if (properties.extent) {
      properties.extent = JSON.parse(properties.extent);
      // this.extent = properties.extent; // si usa il metodo goToExtent perche questo riposiziona senza animazione
      this.mapService.goToExtent(this.mapService.getMap(), properties.extent);

      // } else if (properties.LON_4326 && properties.LAT_4326) {
      //   this.mapService.goToCenter(this.mapService.getMap(), [properties.LON_4326, properties.LAT_4326], 13);
    }
    if (properties.nr_interventi_pubblici) {
      properties.nr_interventi_pubblici = JSON.parse(properties.nr_interventi_pubblici);
    }
    if (properties.totale_interventi_pubblici) {
      properties.totale_interventi_pubblici = JSON.parse(properties.totale_interventi_pubblici);
    }
    if (properties.totale_assegnato) {
      properties.totale_assegnato = JSON.parse(properties.totale_assegnato);
    }
    if (properties.sal) {
      properties.sal = JSON.parse(properties.sal);
    }
    if (properties.pagamenti) {
      properties.pagamenti = JSON.parse(properties.pagamenti);
    }

    this.selected = properties;

    this.prepareInfoboxHeader();

    if (!this.enableDrag) {
      this.intervento = this.selected;
    } else {
      this.intervento = false;
    }
    //console.log('SELECTED: ', this.selected);

  }

  animate() {
    this.isAnimated = true;
    setTimeout(() => {
      this.isAnimated = false;
    }, 1000);
  }

  updateDisplay() {
    if (window.innerWidth > 992 && !this.isMobile) {
      this.enableDrag = false;
      document.getElementsByClassName('info-wrapper')[0].removeAttribute('style');
    } else {
      this.enableDrag = true;
    }
  }

  ngOnDestroy() {
    // avoid memory leaks here by cleaning up after ourselves
    // If we don't then we will continue to run our initialiseInvites() method on every navigationEnd event.
    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
    }
    if (this.mouseClickSubscription) {
      this.mouseClickSubscription.unsubscribe();
    }
  }

  retriveInfo() {
    if (this.selected && !this.intervento) {
      this.intervento = this.selected;
    } else {
      this.intervento = false;
    }
  }

  dragStarted(event: CdkDragStart) {
    this.dragged = true;
    if (!this.intervento) {
      this.dragPosition = { x: 0, y: 0 };
      this.retriveInfo();
    } else {
      this.dragPosition = { x: 0, y: (175 - window.innerHeight) };
    }
  }

  dragEnded(event: CdkDragEnd) {
    if (event.distance.y > 1) {
      this.intervento = false;
      event.source.element.nativeElement.classList.remove('onTop');
    } else {
      event.source.element.nativeElement.classList.add('onTop');
    }
    this.dragged = false;
    this.dragPosition = { x: 0, y: 0 };
  }

  infoExpanded(event) {
    if (!['U', 'A', 'B'].includes(event.target.tagName) && this.enableDrag === true) {
      this.retriveInfo();
      if (this.intervento === false) {
        document.getElementsByClassName('info-wrapper')[0].classList.remove('onTop');
      } else {
        document.getElementsByClassName('info-wrapper')[0].classList.add('onTop');
      }
      this.dragged = false;
      this.dragPosition = { x: 0, y: 0 };
    }
  }

  onResize(event) {
    this.updateDisplay(); // window width
  }

  prepareInfoboxHeader() {
    this.importo_interventi = 0; this.nr_interventi = 0; this.totale_assegnato = 0;this.sal = 0;this.pagamenti = 0;

    this.active_financig_source.forEach((fs) => {
      // console.log('FS: ',fs);
      // console.log('SELECTED: ', this.selected);

      if (this.selected.totale_interventi_pubblici) {
        if (this.selected.totale_interventi_pubblici[fs]) {
          this.importo_interventi += this.selected.totale_interventi_pubblici[fs];
        }
      }

      if (this.selected.nr_interventi_pubblici) {
        if (this.selected.nr_interventi_pubblici[fs]) {
          this.nr_interventi += this.selected.nr_interventi_pubblici[fs];
        }
      }

      if (this.selected.totale_assegnato) {
        if (this.selected.totale_assegnato[fs]) {
          this.totale_assegnato += this.selected.totale_assegnato[fs];
        }
      }

      if (this.selected.sal) {
        if (this.selected.sal[fs]) {
          this.sal += this.selected.sal[fs];
        }
      }

      if (this.selected.pagamenti) {
        if (this.selected.pagamenti[fs]) {
          this.pagamenti += this.selected.pagamenti[fs];
        }
      }
    });
    console.log('SAL:', this.sal);
    console.log('PAGAMENTI:', this.pagamenti);

  }

  onFinancingSourcesFiltering($event) {
    this.active_financig_source = $event;
    this.prepareInfoboxHeader();
  }

  /*
    dragMoved(event: CdkDragMove) {
      console.log(`> Position X: ${event.pointerPosition.x} - Y: ${event.pointerPosition.y}`);
    }*/
}
