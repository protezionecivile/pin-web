import {Component, ElementRef, Input, OnChanges, SimpleChanges, OnInit } from '@angular/core';
import { DeviceDetectorService } from 'ngx-device-detector';

declare function ConicGradient(options: any ): void ;

@Component({
  selector: 'app-radial-graph',
  templateUrl: './radial-graph.component.html',
  styleUrls: ['./radial-graph.component.scss']
})
export class RadialGraphComponent implements OnInit, OnChanges {
  deviceInfo = null;
  @Input() data: any;

  constructor(private _eref: ElementRef,
              private deviceService: DeviceDetectorService) {
    this.deviceInfo = this.deviceService.getDeviceInfo();
  }

  ngOnInit() {
    console.log('INIT - temp:',this.data.temp,' totale:',this.data.totale);
    const per = Math.floor(100 - ((this.data.temp / this.data.totale) * 100));
    this.animation(100, per);
  }

  ngOnChanges(): void {
    console.log('CHANGE - temp:',this.data.temp,' totale:',this.data.totale);
    const per = Math.floor(100 - ((this.data.temp / this.data.totale) * 100));
    this.animation(100, per);
  }

  animation( i , j): any {
    if (this.deviceInfo.browser === 'Firefox') {
      const gradient = new ConicGradient({
        stops: '#919191 ' + j + '%, #033162 0%', // required
        size: 600 // Default: Math.max(innerWidth, innerHeight)
      });
      this._eref.nativeElement.querySelector('.radial').setAttribute('style', 'background-image: url(' + gradient.dataURL + ');');
    } else {
      if ( i > j ) {
        this._eref.nativeElement.querySelector('.radial').setAttribute('style', 'background-image: conic-gradient(#919191 ' + i + '%, #033162 0%);');
        i--;
        setTimeout( () => {this.animation( i , j ); } , 3);
      }
    }

  }

}
