import { Component, Input, OnInit, OnChanges, SimpleChanges, ElementRef } from '@angular/core';
import FunnelGraph from 'node_modules/funnel-graph-js/dist/js/funnel-graph.js';

@Component({
  selector: 'app-funnel',
  templateUrl: './funnel.component.html',
  styleUrls: ['./funnel.component.scss']
})

export class FunnelComponent implements OnInit, OnChanges {

  @Input('data') data: any;
  @Input('actives') actives: any;

  graph: any;
  colors = [
    '#0A3871',
    '#FBB481',
    '#7BD5B9',
    '#DCF26F',
    '#ee6055',
    '#78c0e0',
    '#f9dec9',
    '#beb8eb',
    '#995d81',
    '#b4b8ab',
    '#90fcf9',
    '#919191'
  ];

  constructor(private _eref: ElementRef) { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.formatData();
  }

  formatData() {
    const values = {
      labels: [],
      subLabels: [],
      colors: this.colors,
      values: []
    };
    const jData = JSON.parse(this.data);

    //console.log(jData);
    //console.log(this.actives);
    // TODO: add here logic to merge the data of the active financing sources
    // from "active" @Input
    // it receive one or more funnel data and display it or simply compute one as a merge
    // of the received funnels data

    let thefunnel = [];

    if (this.actives.length == 1) {
      thefunnel = jData[this.actives[0]];
    } else {
      //get the first element and then recompose and sum the values
      //for the subfunnels for the other financing sources
      //console.log(jData[this.actives[0]]);

      if (jData[this.actives[0]] ) {
        thefunnel = jData[this.actives[0]];
      } else {
        thefunnel = jData[this.actives[1]];
      }

      // for each subfunnel
      thefunnel.forEach((subf) => {
        for (let i = 1; i < this.actives.length; i++) {
          let nfun = jData[this.actives[i]];
          if (nfun) {

            nfun.forEach((element) => {
              if (element.subfunnel === subf.subfunnel) {
                element.values.forEach((step, idx) => {
                  subf.values[idx].value = subf.values[idx].value + step.value;
                });
              }
            });
          }

        }
      });
    }

    if (thefunnel) {
      thefunnel[0].values.forEach((item) => {
        values.values.push([]);
        values.labels.push(item.name);
      });

      thefunnel.forEach((item) => {
        values.subLabels.push(item.subfunnel);
        item.values.forEach((sv, index) => {
          values.values[index].push(sv.value);
        });
      });

    }

    if (this.graph !== undefined) {
      this._eref.nativeElement.querySelector('#funnel').innerHTML = '';
    }

    this.graph = new FunnelGraph({
      container: '#funnel',
      data: values,
      displayPercent: true,
      direction: 'vertical',
      width: 180,
      height: 480,
      subLabelValue: 'percent',
      postValueLabel: ' €'
    });
    
    this.graph.draw();

  }

}
