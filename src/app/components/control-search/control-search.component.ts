import { Component, ViewChild, ElementRef, Injector, Injectable } from '@angular/core';
import { ControlSearchComponent, NgMapService, NominatimService, WFSService } from '@sciamlab/ng-map';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { SnackbarService } from '@sciamlab/ng-common';
import { COMUNI, PROVINCE, REGIONI, NS, INTERVENTI } from 'src/config';
import { HttpClient } from '@angular/common/http';
import { Observable, of, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'pin-map-control-search',
  templateUrl: './control-search.component.html',
  styleUrls: ['./control-search.component.scss'],
  host: {
    '(document:click)': 'onClick($event)',
  },
})
export class PINControlSearchComponent extends ControlSearchComponent {
  opened: boolean;
  foo: boolean;

  @ViewChild('searchInput', { static: false }) nameField: ElementRef;

  constructor(
    route: ActivatedRoute,
    router: Router,
    snackbarService: SnackbarService,
    translate: TranslateService,
    mapService: NgMapService,
    // injector: Injector,
    private wfsService: WFSService,
    private _eref: ElementRef,
    http: HttpClient,
  ) {
    super(route, router, snackbarService, translate, mapService, http);
    this.opened = false;
  }

  ngOnInit() {
    try {
      this.search_service = new PinSearchService(this.http, this.wfsService);
      // this.search_service = SearchServiceFactory.build('nominatim', this.http); 
      console.log('SciamLab Map Search Service', this.search_service);

      // Get the current map
      this.map = this.mapService.getMap(this.map_id || 'map');
      if (!this.map) {
        this.mapCreateSubscription = this.mapService.mapCreated$.subscribe(
          map => {
            this.map = map;
          });
      }

    } catch (error) {
      console.error(error);
      this.snackbarService.error(error.message || error);
    }
  }

  toggleSearch(): void {
    this.opened = !this.opened;
    if (this.opened) {
      this.nameField.nativeElement.focus();
    }

  }

  onClick(event) {
    if (!this._eref.nativeElement.contains(event.target)) {
      this.opened = false;
    } else if (event.target.closest('.search-link')) {
      this.toggleSearch();
    }
  }

  formatter = (item: any) => {
    try {

      if (item.properties_ && item.properties_.layer === INTERVENTI) {
        this.mapService.emitFeatureSelect([item]);

      } else {
        const osmid = this.search_service.getOsmId(item);
        // COMUNI
        this.wfsService.getFeatureByOsmId(NS, COMUNI, osmid).toPromise()
          .then(feature => {
            if (feature) {
              this.mapService.emitFeatureSelect([feature]);

            } else {
              // PROVINCE
              this.wfsService.getFeatureByOsmId(NS, PROVINCE, osmid).toPromise()
                .then(feature => {
                  if (feature) {
                    this.mapService.emitFeatureSelect([feature]);

                  } else {
                    // REGIONI
                    this.wfsService.getFeatureByOsmId(NS, REGIONI, osmid).toPromise()
                      .then(feature => {
                        if (feature) {
                          this.mapService.emitFeatureSelect([feature]);

                        } else {
                          // NON HO TROVATO LA SHAPE CON OSM_ID, ritorno l'extent dato dal search_service
                          const extent: number[][] = this.search_service.getExtent(item);
                          this.mapService.goToExtent(this.map, extent);
                        }
                      })
                      .catch(err => { throw err; });
                  }
                })
                .catch(err => { throw err; });
            }
          })
          .catch(err => { throw err; });
      }

      return '';

    } catch (error) {
      console.error(error);
      this.snackbarService.error(error.message || error);
    }
  }
}

@Injectable({
  providedIn: 'root'
})
export class PinSearchService extends NominatimService {
  constructor(
    http: HttpClient,
    private wfsService: WFSService,
  ) {
    super(http);
  }

  search(term: string): Observable<any[]> {
    if (term === '') {
      return of([]);
    }

    // esegue in parallelo la ricerca su WFS e su Nominatim
    const interventi = this.wfsService.getFeaturesByTerm(NS, INTERVENTI, term.toUpperCase());
    const nominatim: Observable<any[]> = super.search(term);

    return forkJoin([interventi, nominatim]).pipe(
      map((results: any[][]) => {
        results[0].forEach(i => {
          i.properties_.extent = JSON.stringify(this.getExtent(i));
        });
        // poi unisce i risultati
        return results[0].concat(results[1]);
      }));
  }

  getExtent(feature: any): number[][] {
    if (feature.properties_ && feature.properties_.layer === INTERVENTI) {
      return [
        [feature.properties_.LON_4326, feature.properties_.LAT_4326],
        [feature.properties_.LON_4326, feature.properties_.LAT_4326]
      ];

    } else {
      return super.getExtent(feature);
    }
  }

  getName(feature: any): string {
    if (feature.properties_ && feature.properties_.layer === INTERVENTI) {
      return `Intervento ${feature.properties_.id_int}`;

    } else {
      return feature.display_name;
    }
  }
}
