import { Component, OnInit } from '@angular/core';
import { AppComponent } from 'src/app/app.component';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  constructor(private rootApp: AppComponent) {
    rootApp.cssClass = 'static-page';
  }


  ngOnInit() {
  }

}
