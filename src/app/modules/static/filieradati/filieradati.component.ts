import { Component, OnInit } from '@angular/core';
import { AppComponent } from 'src/app/app.component';

@Component({
  selector: 'app-filieradati',
  templateUrl: './filieradati.component.html',
  styleUrls: ['./filieradati.component.scss']
})
export class FilieraDatiComponent implements OnInit {

  constructor(private rootApp: AppComponent) {
    rootApp.cssClass = 'static-page';
  }

  ngOnInit() {
  }

}
