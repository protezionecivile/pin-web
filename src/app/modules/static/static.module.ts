/**
 * pin-web
 *
 * Amaca CivicEvidence
 * (c) 2019 by Sciamlab s.r.l.
 *
 * The PIN application is part of the Amaca CivicEvidence Platform
 *
 */
import { NgModule } from '@angular/core';

import { AboutComponent } from './about/about.component';
import { FaqComponent } from './faq/faq.component';

import { StaticRoutingModule } from './static-routing.module';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { CreditsComponent } from './credits/credits.component';
import { FilieraDatiComponent } from './filieradati/filieradati.component';
import { OpenDataComponent } from './opendata/opendata.component';

@NgModule({
  declarations: [
    AboutComponent,
    FaqComponent,
    CreditsComponent,
    FilieraDatiComponent,
    OpenDataComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    NgbModule,
    HttpClientModule,
    FontAwesomeModule,

    StaticRoutingModule
  ],
  exports: [
  ]
})
export class StaticModule { }
