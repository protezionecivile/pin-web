import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AboutComponent } from './about/about.component';
import { FaqComponent } from './faq/faq.component';
import { CreditsComponent } from './credits/credits.component';
import { FilieraDatiComponent } from './filieradati/filieradati.component';
import { OpenDataComponent } from './opendata/opendata.component';

const routes: Routes = [
  {
    path: 'about',
    component: AboutComponent
  },
  {
    path: 'filieradati',
    component: FilieraDatiComponent
  },
  {
    path: 'opendata',
    component: OpenDataComponent
  },
  {
    path: 'credits',
    component: CreditsComponent
  },
  {
    path: 'faq',
    component: FaqComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class StaticRoutingModule { }