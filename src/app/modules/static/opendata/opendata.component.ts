import { Component, OnInit } from '@angular/core';
import { AppComponent } from 'src/app/app.component';

@Component({
  selector: 'app-opendata',
  templateUrl: './opendata.component.html',
  styleUrls: ['./opendata.component.scss']
})
export class OpenDataComponent implements OnInit {

  constructor(private rootApp: AppComponent) {
    rootApp.cssClass = 'static-page';
  }

  ngOnInit() {
  }

}
