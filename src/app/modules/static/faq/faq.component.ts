import { Component, OnInit } from '@angular/core';
import { ViewEncapsulation } from '@angular/compiler/src/core';
import { AppComponent } from 'src/app/app.component';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FaqComponent implements OnInit {

  constructor(private rootApp: AppComponent) {
    rootApp.cssClass = 'static-page';
  }

  ngOnInit() {
  }

}
