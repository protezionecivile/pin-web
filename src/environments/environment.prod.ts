export const environment = {
  production: true,
  logger_level: 'INFO',
  api: {
    url: 'http://localhost:8085'
  }
};
