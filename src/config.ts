import { environment } from './environments/environment';
import { Style } from 'ol/style';
import Icon from 'ol/style/Icon';

export const confini_amministrativi_style = {
    stroke: {
        color: 'rgba(3,	49,	98, .5)',
        width: 1,
        // lineDash: [1, 5]
    },
    fill: { color: 'rgba(0, 0, 0, 0.0)' }
};

export const icon_1028 = new Icon({
    src: 'assets/img/pin_ol1028.png',
    size: [20, 30],
    anchor: [0.5, 1],
});

export const icon_24q = new Icon({
    src: 'assets/img/pin_ol24q.png',
    size: [20, 30],
    anchor: [0.5, 1],
});


export function getStyle(feature) {
    return [new Style({
        image: feature.properties_.FONTE_FINANZIAMENTO == 'LN145' ? icon_1028 : icon_24q,
    })];
}

export const VERSION_UPDATE = '<br /><strong>Dati PIN aggiornati al 15 Novembre 2019</strong>';
export const NS = 'pin';
export const ITALIA = 'italia';
export const MACROREGIONI = 'macroregioni';
export const REGIONI = 'regioni';
export const PROVINCE = 'province';
export const COMUNI = 'comuni';
export const INTERVENTI = 'int_new';
export const REALV = 'rv1';

export const SHOW_ATTRIBUTION = true;


export const CONFIG = {
    date_format: 'dd/MM/yyyy H:mm:ss',
    view: {
        //center: [0,0],
        //zoom: 1,
        // projection: 'EPSG:3857',
        // resolutions: [],
        // maxResolution: 2500,
        // minResolution: 10,
        minZoom: 5,
        maxZoom: 14
    },
    snackbar: {
        duration: 3000
    },
    geoserver: {
        layers: {
            skobbler: {
                id: 'skobbler',
                source: {
                    url: 'https://tiles{1-4}-bc7b4da77e971c12cb0e069bffcf2771.skobblermaps.com/TileService/tiles/2.0/01021113210/7/{z}/{x}/{y}.png',
                    attributions: 'Tiles courtesy Skobbler — Map data OpenStreetMap' + VERSION_UPDATE,
                    crossOrigin: 'anonymous'
                },
                visible: false
            },
            gmaps: {
                id: 'gmaps',
                source: {
                    url: 'https://mt{0-3}.google.com/vt/lyrs=y&x={x}&y={y}&z={z}',
                    attributions: 'Map Data &copy;2019 Google' + VERSION_UPDATE,
                    crossOrigin: 'anonymous'
                },
                visible: true
            },
            // yedda: {
            //     source: {
            //         url: 'https://{a-f}.tile.openstreetmap.se/hydda/full/{z}/{x}/{y}.png',
            //         //crossOrigin: 'anonymous',
            //         attributions: 'Tiles courtesy OSM Sweden — Map data OpenStreetMap'
            //     },
            //     // minResolution: 305
            //     minResolution: 611
            // },
            // esri_grey: {
            //     source: {
            //         url: 'https://services.arcgisonline.com/arcgis/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/{z}/{y}/{x}',
            //         crossOrigin: 'anonymous',
            //         attributions: 'Tiles © <a href="https://services.arcgisonline.com/arcgis/rest/services/Canvas/World_Light_Gray_Base/MapServer/" target="_blank">ArcGIS</a>'
            //     },
            //     // minResolution: 305
            //     minResolution: 611
            // },
            // esri_grey_labels: {
            //     source: {
            //         url: 'https://services.arcgisonline.com/arcgis/rest/services/Canvas/World_Light_Gray_Reference/MapServer/tile/{z}/{y}/{x}',
            //         crossOrigin: 'anonymous',
            //     },
            //     // minResolution: 305
            //     minResolution: 611
            // },
            // esri_topo: {
            //     id: 'esri_topo',
            //     source: {
            //         url: 'https://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}',
            //         crossOrigin: 'anonymous',
            //         attributions: 'Tiles © <a href="https://services.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer" target="_blank">ArcGIS</a>' + VERSION_UPDATE
            //     },
            //     // maxResolution: 304
            //     visible: false,
            //     //maxResolution: 610
            // },
            realvista: {
                id: 'realvista',
                source: {
                    url: '/geoserver/wms',
                    params: {
                        LAYERS: `${NS}:${REALV}`,
                        FORMAT: 'image/jpeg',
                        TILED: true,
                    },
                    serverType: 'geoserver',
                    attributions: '<a href="http://www.realvista.it/">RealVista</a> 1.0 WMS OPEN di e-GEOS S.p.A.' + VERSION_UPDATE
                },
                visible: false
            },
            bing: {
                id: 'bing',
                source: {
                    culture: 'it-it',
                    key: 'AhgrNIiCBw8w1MmHxNcFJ0Kg0UKzml7zhuk1POj70YZ8Uv0zDEk6Po1nMz7YpYsh',
                    imagerySet: 'AerialWithLabelsOnDemand'
                },
                visible: false
            },
            macroregioni: {
                id: 'macroregioni',
                source: {
                    url: `/geoserver/gwc/service/tms/1.0.0/${NS}:${MACROREGIONI}@EPSG:3857@pbf/{z}/{x}/{-y}.pbf`
                },
                minResolution: 2445,
                style: confini_amministrativi_style
            },
            regioni: {
                id: 'regioni',
                source: {
                    url: `/geoserver/gwc/service/tms/1.0.0/${NS}:${REGIONI}@EPSG:3857@pbf/{z}/{x}/{-y}.pbf`
                },
                maxResolution: 2444,
                minResolution: 1222,
                style: confini_amministrativi_style
            },
            province: {
                id: 'province',
                source: {
                    url: `/geoserver/gwc/service/tms/1.0.0/${NS}:${PROVINCE}@EPSG:3857@pbf/{z}/{x}/{-y}.pbf`
                },
                maxResolution: 1221,
                minResolution: 305,
                style: confini_amministrativi_style
            },
            comuni: {
                id: 'comuni',
                source: {
                    url: `/geoserver/gwc/service/tms/1.0.0/${NS}:${COMUNI}@EPSG:3857@pbf/{z}/{x}/{-y}.pbf`
                },
                maxResolution: 304,
                style: confini_amministrativi_style
            },
            interventi_wms: {
                id: 'interventi_wms',
                source: {
                    url: '/geoserver/wms',
                    params: {
                        LAYERS: `${NS}:${INTERVENTI}`,
                        TILED: true,
                        CQL_FILTER: "FONTE_FINANZIAMENTO='LN145' OR FONTE_FINANZIAMENTO='DL119'"
                    },
                    serverType: 'geoserver'
                },
                minResolution: 305
            },
            interventi_mvt_LN145: {
                id: 'interventi_mvtLN145',
                source: {
                    url: `/geoserver/gwc/service/tms/1.0.0/${NS}:${INTERVENTI}LN145@EPSG:3857@pbf/{z}/{x}/{-y}.pbf`,
                },
                maxResolution: 304,
                declutter: true,
                style: getStyle,
                visibile: true
            },
            interventi_mvt_DL119: {
                id: 'interventi_mvtDL119',
                source: {
                    url: `/geoserver/gwc/service/tms/1.0.0/${NS}:${INTERVENTI}DL119@EPSG:3857@pbf/{z}/{x}/{-y}.pbf`,
                },
                maxResolution: 304,
                declutter: true,
                style: getStyle,
                visibile: true
            }

        },
        wfs: {
            url: '/geoserver/pin/ows'
        }
    },
    api: {
        url: environment.api.url
    },
    simog_url:'http://portaletrasparenza.anticorruzione.it/Microstrategy/asp/Main.aspx?evt=2048001&src=Main.aspx.2048001&visMode=0&hiddenSections=header,footer,path,dockTop&documentID=0E392EF94E86CCDD246176A3580200AB&currentViewMedia=2&Main.aspx=-10*.119*.128*.95.SISk*_Extranet.0_&shared=*-1.*-1.0.0.0&ftb=0.422541B24E28B69DC5DF858B20E67091.*0.8.0.0-8.18_268453447.*-1.1.*0&fb=0.422541B24E28B69DC5DF858B20E67091.Extranet.8.0.0-8.768.769.774.770.773.772.775.55.256.10.257.776.777_268453447.*-1.1.*0&uid=web&pwd=no&valuePromptAnswers='
};
