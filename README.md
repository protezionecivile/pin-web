# pin-web

  P.I.N. is an application developed for the Department of Civil Protection
  which makes the information available on the National Intervention Plan 
  against hydrogeological instability for the safety of the territory and 
  risk prevention.

  --
  
  P.I.N. è un applicativo sviluppato per il Dipartimento della Protezione Civile
  che rende disponibili le informazioni del Piano Interventi Nazionale contro
  il dissesto idrogeologico per la messa in sicurezza del territorio e 
  prevenzione del rischio.


## License
This content is released under the GNU Affero General Public License v.3
For further information please read the [LICENSE](LICENSE) file. For 3rd party libraries
and licenses please read the [LICENSES-3RD-PARTIES](LICENSES-3RD-PARTIES)
